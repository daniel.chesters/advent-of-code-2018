package fr.oni.dev.adventOfCode2018.day2;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class Day2Test {
    Day2 day2 = new Day2();
    private List<String> ids = List.of("abcdef", "bababc", "abbcde", "abcccd", "aabcdd", "abcdee", "ababab");

    private List<String> idsPart2 = List.of("abcde", "fghij", "klmno", "pqrst", "fguij", "axcye", "wvxyz");

    @Test
    void day2HasTwoSameLetters1() {
        assertEquals(false, day2.hasTwoSameLetters("abcdef"));
    }

    @Test
    void day2HasTwoSameLetters2() {
        assertEquals(true, day2.hasTwoSameLetters("bababc"));
    }

    @Test
    void day2HasTwoSameLetters3() {
        assertEquals(true, day2.hasTwoSameLetters("abbcde"));
    }

    @Test
    void day2HasTwoSameLetters4() {
        assertEquals(false, day2.hasTwoSameLetters("abcccd"));
    }

    @Test
    void day2HasTwoSameLetters5() {
        assertEquals(true, day2.hasTwoSameLetters("aabcdd"));
    }

    @Test
    void day2HasTwoSameLetters6() {
        assertEquals(true, day2.hasTwoSameLetters("abcdee"));
    }

    @Test
    void day2HasTwoSameLetters7() {
        assertEquals(false, day2.hasTwoSameLetters("ababab"));
    }

    @Test
    void day2HasThreeSameLetters1() {
        assertEquals(false, day2.hasThreeSameLetters("abcdef"));
    }

    @Test
    void day2HasThreeSameLetters2() {
        assertEquals(true, day2.hasThreeSameLetters("bababc"));
    }

    @Test
    void day2HasThreeSameLetters3() {
        assertEquals(false, day2.hasThreeSameLetters("abbcde"));
    }

    @Test
    void day2HasThreeSameLetters4() {
        assertEquals(true, day2.hasThreeSameLetters("abcccd"));
    }

    @Test
    void day2HasThreeSameLetters5() {
        assertEquals(false, day2.hasThreeSameLetters("aabcdd"));
    }

    @Test
    void day2HasThreeSameLetters6() {
        assertEquals(false, day2.hasThreeSameLetters("abcdee"));
    }

    @Test
    void day2HasThreeSameLetters7() {
        assertEquals(true, day2.hasThreeSameLetters("ababab"));
    }

    @Test
    void day2CountHasTwoSameLetters() {
        assertEquals(4, day2.countHasTwoSameLetters(ids));
    }

    @Test
    void day2CountHasThreeSameLetters() {
        assertEquals(3, day2.countHasThreeSameLetters(ids));
    }

    @Test
    void day2FoundRightId() {
        assertEquals("fgij", day2.foundRightId(idsPart2));
    }
}