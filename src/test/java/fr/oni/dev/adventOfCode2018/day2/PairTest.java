package fr.oni.dev.adventOfCode2018.day2;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PairTest {

    @Test
    void countDiff1() {
        var pair = new Pair("abcde", "axcye");
        assertEquals(2, pair.countDiff());
    }

    @Test
    void countDiff2() {
        var pair = new Pair("fghij", "fguij");
        assertEquals(1, pair.countDiff());
    }

    @Test
    void removeDiff1() {
        var pair = new Pair("abcde", "axcye");
        assertEquals("ace", pair.removeDiff());
    }

    @Test
    void removeDiff2() {
        var pair = new Pair("fghij", "fguij");
        assertEquals("fgij", pair.removeDiff());
    }
}