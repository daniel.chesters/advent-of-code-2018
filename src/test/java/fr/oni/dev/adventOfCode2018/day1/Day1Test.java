package fr.oni.dev.adventOfCode2018.day1;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class Day1Test {
    private Day1 day1 = new Day1();

    @Test
    void day1ComputeFrequency1() {
        assertEquals(3, day1.computeFrequency(List.of(1,-2, 3, 1)));
    }

    @Test
    void day1ComputeFrequency2() {
        assertEquals(3, day1.computeFrequency(List.of(1, 1, 1)));
    }

    @Test
    void day1ComputeFrequency3() {
        assertEquals(0, day1.computeFrequency(List.of(1, 1, -2)));
    }

    @Test
    void day1ComputeFrequency4() {
        assertEquals(-6, day1.computeFrequency(List.of(-1, -2, -3 )));
    }

    @Test
    void day1FindFirstTwice1() {
        assertEquals(0, day1.findFirstTwice(List.of(1, -1)));
    }

    @Test
    void day1FindFirstTwice2() {
        assertEquals(10, day1.findFirstTwice(List.of(3, 3, 4, -2, -4)));
    }

    @Test
    void day1FindFirstTwice3() {
        assertEquals(5, day1.findFirstTwice(List.of(-6, 3, 8, 5, -6)));
    }

    @Test
    void day1FindFirstTwice4() {
        assertEquals(14, day1.findFirstTwice(List.of(7,7,-2,-7,-4)));
    }
}