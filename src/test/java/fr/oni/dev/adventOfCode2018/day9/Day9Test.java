package fr.oni.dev.adventOfCode2018.day9;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Day9Test {

    Day9 day9 = new Day9();

    @Test
    void computeHighScore1() {
        assertEquals(8317, day9.computeHighScore(10, 1618));
    }

    @Test
    void computeHighScore2() {
        assertEquals(146373, day9.computeHighScore(13, 7999));
    }

    @Test
    void computeHighScore3() {
        assertEquals(32, day9.computeHighScore(9, 25));
    }

    @Test
    void computeHighScore4() {
        assertEquals(2764, day9.computeHighScore(17, 1104));
    }

    @Test
    void computeHighScore5() {
        assertEquals(54718, day9.computeHighScore(21, 6111));
    }

    @Test
    void computeHighScore6() {
        assertEquals(37305, day9.computeHighScore(30, 5807));
    }
}