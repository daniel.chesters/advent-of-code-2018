package fr.oni.dev.adventOfCode2018.day3;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Day3Test {
    Day3 day3 = new Day3();

    @Test
    void readClaimRectangle1() {
        var claimRectangle = day3.readClaimRectangle("#1 @ 1,3: 4x4");
        assertEquals(1, claimRectangle.getX());
        assertEquals(3, claimRectangle.getY());
        assertEquals(4, claimRectangle.getWidth());
        assertEquals(4, claimRectangle.getHeight());
        assertEquals("#1", claimRectangle.getId());
    }

    @Test
    void readClaimRectangle2() {
        var claimRectangle = day3.readClaimRectangle("#2 @ 3,1: 4x4");
        assertEquals(3, claimRectangle.getX());
        assertEquals(1, claimRectangle.getY());
        assertEquals(4, claimRectangle.getWidth());
        assertEquals(4, claimRectangle.getHeight());
        assertEquals("#2", claimRectangle.getId());
    }

    @Test
    void readClaimRectangle3() {
        var claimRectangle = day3.readClaimRectangle("#3 @ 5,5: 2x2");
        assertEquals(5, claimRectangle.getX());
        assertEquals(5, claimRectangle.getY());
        assertEquals(2, claimRectangle.getWidth());
        assertEquals(2, claimRectangle.getHeight());
        assertEquals("#3", claimRectangle.getId());
    }

    @Test
    void countClaimsTwice() {
        var claims = List.of("#1 @ 1,3: 4x4", "#2 @ 3,1: 4x4", "#3 @ 5,5: 2x2");
        var claimRectangles = day3.generateListOfClaim(claims);
        var claimSquares = day3.initializationListOfClaims();
        claimRectangles.forEach(claimRectangle -> day3.addRectangleInClaims(claimSquares, claimRectangle));
        assertEquals(4, day3.countClaimsTwiceOrMore(claimSquares));
    }

    @Test
    void findIdWithoutTwiceClaims() {
        var claims = List.of("#1 @ 1,3: 4x4", "#2 @ 3,1: 4x4", "#3 @ 5,5: 2x2");
        var claimRectangles = day3.generateListOfClaim(claims);
        var claimSquares = day3.initializationListOfClaims();
        claimRectangles.forEach(claimRectangle -> day3.addRectangleInClaims(claimSquares, claimRectangle));
        assertEquals("#3", day3.findRectangleWithoutTwiceClaims(claimSquares, claimRectangles));
    }
}
