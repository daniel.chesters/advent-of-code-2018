package fr.oni.dev.adventOfCode2018.day11;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Day11Test {

    Day11 day11 = new Day11();

    @Test
    void computePowerLevel1() {
        assertEquals(-5, day11.computePowerLevel(122, 79, 57));
    }

    @Test
    void computePowerLevel2() {
        assertEquals(0, day11.computePowerLevel(217, 196, 39));
    }

    @Test
    void computePowerLevel3() {
        assertEquals(4, day11.computePowerLevel(101, 153, 71));
    }

    @Test
    void findOptimalPowerLevel3by3_1() {
        assertEquals("33,45", day11.findOptimal3by3PowerLevel(18));
    }

    @Test
    void findOptimalPowerLevel3by3_2() {
        assertEquals("21,61", day11.findOptimal3by3PowerLevel(42));
    }

    @Test
    @Disabled
    void findOptimalSquarePowerLevel1() {
        assertEquals("90,269,16", day11.findOptimalSquarePowerLevel(18));
    }

    @Test
    @Disabled
    void findOptimalSquarePowerLevel2() {
        assertEquals("232,251,12", day11.findOptimalSquarePowerLevel(42));
    }
}