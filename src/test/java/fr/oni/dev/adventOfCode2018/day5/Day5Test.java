package fr.oni.dev.adventOfCode2018.day5;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Day5Test {
    Day5 day5 = new Day5();
    private String polymer = "dabAcCaCBAcCcaDA";

    @Test
    void polymerReact1() {
        assertEquals("", day5.polymerReact("aA"));
    }

    @Test
    void polymerReact2() {
        assertEquals("", day5.polymerReact("abBA"));
    }

    @Test
    void polymerReact3() {
        assertEquals("abAB", day5.polymerReact("abAB"));
    }

    @Test
    void polymerReact4() {
        assertEquals("aabAAB", day5.polymerReact("aabAAB"));
    }

    @Test
    void polymerReact5() {
        assertEquals("dabCBAcaDA", day5.polymerReact(polymer));
    }

    @Test
    void removeAndReact1() {
        assertEquals("dbCBcD", day5.removeAndReact(polymer, 'a'));
    }

    @Test
    void removeAndReact2() {
        assertEquals("daCAcaDA", day5.removeAndReact(polymer, 'b'));
    }

    @Test
    void removeAndReact3() {
        assertEquals("daDA", day5.removeAndReact(polymer, 'c'));
    }

    @Test
    void removeAndReact4() {
        assertEquals("abCBAc", day5.removeAndReact(polymer, 'd'));
    }

    @Test
    void findMostEffectiveReact() {
        assertEquals("daDA", day5.findMostEffectiveReact(polymer));
    }
}