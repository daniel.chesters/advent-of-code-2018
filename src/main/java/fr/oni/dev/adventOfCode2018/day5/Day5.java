package fr.oni.dev.adventOfCode2018.day5;

import fr.oni.dev.adventOfCode2018.Utils;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

//TODO To see if we can improve performance
public class Day5 {
    public static void main(String[] args) {
        var polymer = Utils.readStringFromFile("input5-1.txt");
        var day5 = new Day5();
        System.out.println("Part 1 : " + day5.polymerReact(polymer).length());
        System.out.println("Part 2 : " + day5.findMostEffectiveReact(polymer).length());
    }

    public String polymerReact(String polymer) {
        var newPolymer = polymer;
        var stop = false;
        do {
            var currentPolymer = newPolymer;
            var firstReact = IntStream.range(1, currentPolymer.length())
                    .filter(i -> currentPolymer.charAt(i - 1) != currentPolymer.charAt(i) &&
                            Character.toLowerCase(currentPolymer.charAt(i - 1)) == Character.toLowerCase(currentPolymer.charAt(i))).findFirst();

            if (firstReact.isPresent()) {
                int react = firstReact.getAsInt();
                newPolymer = IntStream.range(0, currentPolymer.length())
                        .filter(i -> i != react - 1 && i != react)
                        .mapToObj(i -> String.valueOf(currentPolymer.charAt(i)))
                        .collect(Collectors.joining());
            } else {
                stop = true;
            }
        } while (!stop);

        return newPolymer;
    }

    public String removeAndReact(String polymer, char c) {
        return polymerReact(IntStream.range(0, polymer.length())
                .filter(i -> polymer.charAt(i) != c && Character.toLowerCase(polymer.charAt(i)) != c)
                .mapToObj(i -> String.valueOf(polymer.charAt(i)))
                .collect(Collectors.joining()));
    }

    public String findMostEffectiveReact(String polymer) {
        var reacts = polymer.chars()
                .mapToObj(c -> (char) c)
                .map(Character::toLowerCase)
                .distinct()
                .map(c -> removeAndReact(polymer, c))
                .collect(Collectors.toList());

        var minLength = reacts.stream().mapToInt(String::length).min().orElse(0);

        return reacts.stream().filter(p -> p.length() == minLength).findFirst().orElse("");
    }
}
