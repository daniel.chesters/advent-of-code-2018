package fr.oni.dev.adventOfCode2018.day2;

import fr.oni.dev.adventOfCode2018.Utils;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Day2 {
    public static void main(String... args) {
        List<String> lines = Utils.readLinesFromFile("input2-1.txt");
        Day2 day2 = new Day2();

        System.out.println("Part 1 : " + day2.countHasTwoSameLetters(lines) * day2.countHasThreeSameLetters(lines));
        System.out.println("Part 2 : " + day2.foundRightId(lines));
    }

    public boolean hasTwoSameLetters(String id) {
        return hasNSameLetters(id, 2);
    }

    private boolean hasNSameLetters(String id, int n) {
        return id.chars()
                .mapToObj(i -> (char) i)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet().stream()
                .anyMatch(e -> e.getValue() == n);
    }

    public boolean hasThreeSameLetters(String id) {
        return hasNSameLetters(id, 3);
    }

    public long countHasTwoSameLetters(List<String> ids) {
        return ids.stream().filter(this::hasTwoSameLetters).count();
    }

    public long countHasThreeSameLetters(List<String> ids) {
        return ids.stream().filter(this::hasThreeSameLetters).count();
    }

    public String foundRightId(List<String> idsPart2) {
        var setOfPairs = generateSetOfPairs(idsPart2);
        Optional<Pair> first = setOfPairs.stream().filter(pair -> pair.countDiff() == 1).findFirst();
        return first.orElseGet(() -> new Pair("","")).removeDiff();
    }

    private Set<Pair> generateSetOfPairs(List<String> ids) {
        var setOfPairs = new HashSet<Pair>();

        for (var left : ids) {
            for (var right : ids) {
                var pair = new Pair(left, right);
                var inversePair = new Pair(right, left);

                if (!left.equals(right) && !setOfPairs.contains(pair) && !setOfPairs.contains(inversePair)) {
                    setOfPairs.add(pair);
                }
            }
        }

        return setOfPairs;
    }
}
