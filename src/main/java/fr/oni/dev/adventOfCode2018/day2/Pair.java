package fr.oni.dev.adventOfCode2018.day2;

import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Pair {

    private String left;
    private String right;

    public Pair(String left, String right) {
        this.left = left;
        this.right = right;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Pair)) return false;
        Pair pair = (Pair) o;
        return Objects.equals(left, pair.left) &&
                Objects.equals(right, pair.right);
    }

    @Override
    public int hashCode() {
        return Objects.hash(left, right);
    }

    public long countDiff() {
        return IntStream.range(0, left.length())
                .filter(i -> left.charAt(i) != right.charAt(i)).count();
    }

    public String removeDiff() {
        return IntStream.range(0, left.length())
                .filter(i -> left.charAt(i) == right.charAt(i))
                .mapToObj(i -> left.charAt(i))
                .map(String::valueOf)
                .collect(Collectors.joining(""));
    }
}
