package fr.oni.dev.adventOfCode2018.day11;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.IntStream;

public class Day11 {
    public static void main(String[] args) {
        var day11 = new Day11();
        var startTime = System.currentTimeMillis();
        System.out.println("Part 1 : " + day11.findOptimal3by3PowerLevel(2187));
        System.out.println("Duration :" + (System.currentTimeMillis() - startTime));
        startTime = System.currentTimeMillis();
        System.out.println("Part 2 : " + day11.findOptimalSquarePowerLevel(2187));
        System.out.println("Duration :" + (System.currentTimeMillis() - startTime));
    }

    public int computePowerLevel(int x, int y, int serialNumber) {
        var rackId = x + 10;
        var powerLevelStart = rackId * y;
        var powerLevelIncreased = powerLevelStart + serialNumber;
        var step4 = powerLevelIncreased * rackId;
        var step5 = (step4 / 100) % 10;
        return step5 - 5;
    }

    private long computePowerLevelSquare(long[][] powerLevel, int x, int y, int size) {
        var xMax = x + (size - 1);
        var yMax = y + (size - 1);

        xMax = (xMax > 299) ? 299 : xMax;
        yMax = (yMax > 299) ? 299 : yMax;

        var firstZone = (x == 0 || y == 0) ? 0 : powerLevel[x - 1][y - 1];
        var secondZone = x == 0 ? 0 : powerLevel[x - 1][yMax];
        var thirdZone = y == 0 ? 0 : powerLevel[xMax][y - 1];
        var fourthZone = powerLevel[xMax][yMax];

        return fourthZone + firstZone - secondZone - thirdZone;

    }

    public String findOptimal3by3PowerLevel(int serialNumber) {
        var powerLevels = getPowerLevels(serialNumber);
        var summedPowerLevels = getSummedPowerLevels(powerLevels);

        var all3by3PowerLevel = new HashMap<String, Long>();

        IntStream.range(0, 300)
                .forEach(x -> IntStream.range(0, 300)
                        .forEach(y -> all3by3PowerLevel.put(x + "," + y,
                                computePowerLevelSquare(summedPowerLevels, x, y, 3))));


        return all3by3PowerLevel.entrySet()
                .stream()
                .max(Comparator.comparingLong(Map.Entry::getValue))
                .orElseThrow()
                .getKey();
    }

    private int[][] getPowerLevels(int serialNumber) {
        var powerLevel = new int[300][300];
        for (var x = 0; x < 300; x++) {
            for (var y = 0; y < 300; y++) {
                powerLevel[x][y] = computePowerLevel(x, y, serialNumber);
            }
        }
        return powerLevel;
    }

    private long[][] getSummedPowerLevels(int[][] powerLevels) {
        var summedPowerLevel = new long[300][300];
        for (var x = 0; x < 300; x++) {
            for (var y = 0; y < 300; y++) {
                summedPowerLevel[x][y] = computeSumPowerLevel(powerLevels, x, y);
            }
        }
        return summedPowerLevel;
    }

    private long computeSumPowerLevel(int[][] powerLevels, int x, int y) {
        return IntStream.rangeClosed(0, x)
                .mapToLong(i -> IntStream.rangeClosed(0, y)
                        .map(j -> powerLevels[i][j])
                        .sum())
                .sum();
    }

    public String findOptimalSquarePowerLevel(int serialNumber) {
        var powerLevels = getPowerLevels(serialNumber);
        var summedPowerLevels = getSummedPowerLevels(powerLevels);

        var allSquarePowerLevel = new HashMap<String, Long>();

        IntStream.range(1, 300)
                .forEach(s ->
                        IntStream.range(0, 300)
                                .forEach(x -> IntStream.range(0, 300)
                                        .forEach(y -> allSquarePowerLevel.put(x + "," + y + "," + s,
                                                computePowerLevelSquare(summedPowerLevels, x, y, s)))));


        return allSquarePowerLevel.entrySet()
                .stream()
                .max(Comparator.comparingLong(Map.Entry::getValue))
                .orElseThrow()
                .getKey();
    }
}
