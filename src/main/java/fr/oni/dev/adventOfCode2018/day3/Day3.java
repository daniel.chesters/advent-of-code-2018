package fr.oni.dev.adventOfCode2018.day3;

import fr.oni.dev.adventOfCode2018.Utils;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.stream.Collectors.toList;

public class Day3 {


    public static void main(String... args) {
        var claims = Utils.readLinesFromFile("input3-1.txt");

        var day3 = new Day3();

        var claimSquares = day3.initializationListOfClaims();
        var claimRectangles = day3.generateListOfClaim(claims);

        claimRectangles.forEach(claimRectangle -> day3.addRectangleInClaims(claimSquares, claimRectangle));

        System.out.println("Part 1 : " + day3.countClaimsTwiceOrMore(claimSquares));
        System.out.println("Part 2 : " + day3.findRectangleWithoutTwiceClaims(claimSquares, claimRectangles));
    }

    public ClaimRectangle readClaimRectangle(String claimString) {
        Pattern pattern = Pattern.compile("(#\\w+)\\s@\\s(\\d+),(\\d+):\\s(\\d+)x(\\d+)");
        Matcher matcher = pattern.matcher(claimString);

        String id = null;
        int x = 0;
        int y = 0;
        int width = 0;
        int height = 0;
        if (matcher.find()) {
            id = matcher.group(1);
            x = Integer.parseInt(matcher.group(2));
            y = Integer.parseInt(matcher.group(3));
            width = Integer.parseInt(matcher.group(4));
            height = Integer.parseInt(matcher.group(5));
        }

        return new ClaimRectangle(id, x, y, width, height);
    }

    public List<ClaimRectangle> generateListOfClaim(List<String> claims) {
        return claims.stream().map(this::readClaimRectangle).collect(toList());
    }

    public Character[][] initializationListOfClaims() {
        Character[][] squares = new Character[1000][];
        for (int x = 0; x < 1000; x++) {
            squares[x] = new Character[1000];
            for (int y = 0; y < 1000; y++) {
                squares[x][y] = '.';
            }
        }
        return squares;
    }

    public void addRectangleInClaims(Character[][] squares, ClaimRectangle claimRectangle) {
        for (var x = claimRectangle.getX(); x < (claimRectangle.getX() + claimRectangle.getWidth()); x++) {
            for (var y = claimRectangle.getY(); y < (claimRectangle.getY() + claimRectangle.getHeight()); y++) {
                switch (squares[x][y]) {
                    case '.':
                        squares[x][y] = '#';
                        break;
                    case '#':
                        squares[x][y] = 'X';
                        break;
                    default:
                        break;
                }
            }
        }
    }

    public long countClaimsTwiceOrMore(Character[][] squares) {
        return Arrays.stream(squares).mapToLong(c -> Arrays.stream(c).filter(s -> s == 'X').count()).sum();
    }

    public String findRectangleWithoutTwiceClaims(Character[][] claimSquares, List<ClaimRectangle> claimRectangles) {
        Optional<ClaimRectangle> first = claimRectangles.stream().filter(claimRectangle -> {
            for (var x = claimRectangle.getX(); x < (claimRectangle.getX() + claimRectangle.getWidth()); x++) {
                for (var y = claimRectangle.getY(); y < (claimRectangle.getY() + claimRectangle.getHeight()); y++) {
                    if (claimSquares[x][y] == 'X') {
                        return false;
                    }
                }
            }
            return true;
        }).findFirst();
        if (first.isPresent()) {
            return first.get().getId();
        }
        return "";
    }
}
