package fr.oni.dev.adventOfCode2018.day3;

import java.util.Objects;

public class ClaimRectangle {
    private String id;
    private int x;
    private int y;
    private int width;
    private int height;

    public ClaimRectangle(String id, int x, int y, int width, int height) {
        this.id = id;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ClaimRectangle)) return false;
        ClaimRectangle that = (ClaimRectangle) o;
        return x == that.x &&
                y == that.y &&
                width == that.width &&
                height == that.height &&
                id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, x, y, width, height);
    }
}
