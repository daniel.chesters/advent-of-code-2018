package fr.oni.dev.adventOfCode2018.day9;

import java.util.Arrays;

public class Day9 {
    public long computeHighScore(int nbPlayer, int lastMarble) {
        var scorePlayers = new long[nbPlayer];
        var currentPlayer = 0;

        var currentMarble = new Marble(0);


        for (var marble = 1; marble <= lastMarble; marble++) {
            if (marble % 23 == 0) {
                scorePlayers[currentPlayer] += marble;

                var toRemoveMarble = currentMarble.previous
                        .previous
                        .previous
                        .previous
                        .previous
                        .previous
                        .previous;

                var previousMarble = toRemoveMarble.previous;
                var nextMarble = toRemoveMarble.next;

                previousMarble.next = nextMarble;
                nextMarble.previous = previousMarble;

                scorePlayers[currentPlayer] += toRemoveMarble.value;

                currentMarble = nextMarble;
            } else {
                var selectedMarble = currentMarble.next;

                var newMarble = new Marble(marble);

                newMarble.previous = selectedMarble;
                newMarble.next = selectedMarble.next;

                selectedMarble.next.previous = newMarble;
                selectedMarble.next = newMarble;

                currentMarble = newMarble;
            }

            currentPlayer++;
            if (currentPlayer >= nbPlayer) {
                currentPlayer = 0;
            }
        }

        return Arrays.stream(scorePlayers).max().orElse(0);
    }

    public static void main(String[] args) {
        var day9 = new Day9();
        long startTime = System.currentTimeMillis();
        System.out.println("Part 1 : " + day9.computeHighScore(476, 71431));
        System.out.println("Duration :" + (System.currentTimeMillis() - startTime));
        startTime = System.currentTimeMillis();
        System.out.println("Part 2 : " + day9.computeHighScore(476, 71431 * 100));
        System.out.println("Duration :" + (System.currentTimeMillis() - startTime));
    }
}
