package fr.oni.dev.adventOfCode2018.day9;

public class Marble {
    public int value;
    public Marble previous;
    public Marble next;

    public Marble(int value) {
        this.value = value;
        if (value == 0) {
            previous = this;
            next = this;
        }
    }
}
