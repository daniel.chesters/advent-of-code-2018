package fr.oni.dev.adventOfCode2018;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

public class Utils {

    public static List<Integer> readIntegerFromFile(String filename) {
        try (var scanner = new Scanner(Objects.requireNonNull(Utils.class.getClassLoader().getResourceAsStream(filename)))) {
            var integers = new ArrayList<Integer>();

            while (scanner.hasNextInt()) {
                integers.add(scanner.nextInt());
            }

            return integers;
        }
    }

    public static List<String> readLinesFromFile(String filename) {
        try (var scanner = new Scanner(Objects.requireNonNull(Utils.class.getClassLoader().getResourceAsStream(filename)))) {
            var lines = new ArrayList<String>();

            while (scanner.hasNextLine()) {
                lines.add(scanner.nextLine());
            }

            return lines;
        }
    }

    public static String readStringFromFile(String filename) {
        return readLinesFromFile(filename).get(0);
    }
}
