package fr.oni.dev.adventOfCode2018.day1;

import fr.oni.dev.adventOfCode2018.Utils;

import java.util.ArrayList;
import java.util.List;

public class Day1 {
    public static void main(String... args) {
        List<Integer> frequencyChanges = Utils.readIntegerFromFile("input1-1.txt");
        Day1 day1 = new Day1();

        System.out.println("Part 1 : " + day1.computeFrequency(frequencyChanges));
        System.out.println("Part 2 : " + day1.findFirstTwice(frequencyChanges));
    }

    public long computeFrequency(List<Integer> frequencyChanges) {
        return frequencyChanges.stream().mapToInt(Integer::intValue).sum();
    }

    public int findFirstTwice(List<Integer> frequencyChanges) {
        var frequencyList = new ArrayList<Integer>();

        boolean hasDuplicate = false;
        int lastFrequency = 0;
        while (!hasDuplicate) {
            for (var i : frequencyChanges) {
                frequencyList.add(lastFrequency);
                var newFrequency = i + lastFrequency;
                hasDuplicate = frequencyList.stream().anyMatch(frequency -> frequency == newFrequency);
                lastFrequency = newFrequency;
                if (hasDuplicate) {
                    break;
                }
            }
        }

        return lastFrequency;
    }
}
