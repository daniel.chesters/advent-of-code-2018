plugins {
    java
}

group = "fr.oni.dev"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    testCompile("org.junit.jupiter:junit-jupiter-api:5.3.2")
    testCompile("org.junit.jupiter:junit-jupiter-params:5.3.2")
    testRuntime("org.junit.jupiter:junit-jupiter-engine:5.3.2")
}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_11
}

tasks.test {
    useJUnitPlatform()
    maxHeapSize = "4g"
}

tasks {
    val runDay1 by registering(JavaExec::class) {
        classpath = sourceSets.main.get().runtimeClasspath
        main = "fr.oni.dev.adventOfCode2018.day1.Day1"
    }
    val runDay2 by registering(JavaExec::class) {
        classpath = sourceSets.main.get().runtimeClasspath
        main = "fr.oni.dev.adventOfCode2018.day2.Day2"
    }
    val runDay3 by registering(JavaExec::class) {
        classpath = sourceSets.main.get().runtimeClasspath
        main = "fr.oni.dev.adventOfCode2018.day3.Day3"
    }
    val runDay5 by registering(JavaExec::class) {
        classpath = sourceSets.main.get().runtimeClasspath
        main = "fr.oni.dev.adventOfCode2018.day5.Day5"
    }
    val runDay9 by registering(JavaExec::class) {
        classpath = sourceSets.main.get().runtimeClasspath
        main = "fr.oni.dev.adventOfCode2018.day9.Day9"
    }
    val runDay11 by registering(JavaExec::class) {
        classpath = sourceSets.main.get().runtimeClasspath
        main = "fr.oni.dev.adventOfCode2018.day11.Day11"
        maxHeapSize = "4g"
    }
}